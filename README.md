Prerequisites
Before getting started, you should review the Best Practices for Running Tests.
You need to have these components installed to set up testing on Sauce with C#.NET:

Visual Studio
Selenium WebDriver
Selenium DLLs for .NET installed and referenced by your project
Installing Selenium WebDriver
You can also download the Selenium driver for C# from the official Selenium HQ website, where you can find additional documentation about working with Selenium.

Open a new project in Visual Studio.
Select a C# class library template.
Give the project a name and click OK.
In the Visual Studio Tools menu, go to Library Package Manager > Manage Nuget Package for Solution. 
This will open the Manage NuGet Packages screen.
Click Online, and then click Next.
In the Search Packages field, enter Selenium and then click Search.
In the search results, select Selenium Webdriver and then click Install.
Installing Selenium DLLs
After you’ve set up your project in Visual Studio, you need to make sure that it references the necessary Selenium DLLs for .NET.

Download the Selenium DLLs for .NET from http://selenium-release.storage.googleapis.com/index.html?path=2.47/
In the Solutions Explorer, select the project and right-click References.
Click Add Reference.
Click Browse and navigate to the net40 folder of the directory where you saved the Selenium .NET DLLs.
Add the WebDriver.Support.dll reference to your project.
Installing NUnit
NUnit is a unit-test framework for all .Net languages, written entirely in C# and designed to take advantage of many .NET language features, for example custom attributes and other reflection-related capabilities. For more information and documentation about the framework, as well as how to use it in your testing, you can visit the official NUnit website.

Download the current stable release of NUnit from http://www.nunit.org/index.php?p=download.
In the Solutions Explorer, select the project and right-click References.
Click Add Reference.
Click Browse and navigate to the bin of the directory where you saved NUnit.
Add the nunit.framework.dll and pnunit.framework.dll reference to your project.

Run SendEmailTest with Selenium

Open test/SendEmail.cs and right click inside ComposeAndSendEmailTest() function and choose Run Tests.
	1. A chrome Incognito browser tab will load and the URL will be supplied of Gmail.
	2. User credentials will be entered and presses Login.
	3. User hits the Compose button.
	4. Fills in the email To, Subject and the body and clicks on Send.
	5. The second user logs in and verifies that the number of messages increased.
	6. He makes sure the message was sent by the predefined user.