﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace SendEmailTests
{
    [TestFixture]
    public class SendEmailTests
    {
        [Test]
        public void ComposeAndSendEmailTest()
        {
            System.Environment.SetEnvironmentVariable("webdriver.chrome.driver", @"C:\sikuli\chromedriver.exe");
            string emailSender = "qaengineer.testing@gmail.com";
            string emailReceiver = "qaengineer.testing.receiver@gmail.com";
            string emailSubject = "Hello from Selenium";
            string emailBody = "Hello. \n\n" +
                    " This is an automated Selenium test to check coding skills \n\n" +
                    " Best Regards, \n" +
                    " C# Selenium Web Driver";
     
            IWebDriver driver = LoginWithSpecifiedUser(emailSender);
            int randomNumber = new Random().Next(int.MinValue, int.MaxValue);
            emailSubject = emailReceiver + " " + randomNumber;

            /*Verify login */
            string title = "Gmail";
            Assert.IsTrue(driver.Title.Contains(title));

            // This is to let all the widgets to laod and then locate Compose
            Thread.Sleep(3000);

            ComposeEmail(emailReceiver, emailSubject, emailBody, driver);

            Thread.Sleep(2000);
            //Logout from Gmail
            driver.Quit();

            IWebDriver driverNew = LoginWithSpecifiedUser(emailReceiver);
            //Verify that the Login was successful
            Thread.Sleep(3000);
            Assert.IsTrue(driverNew.Title.Contains("gmail"));

            Thread.Sleep(2000);

            //Verify that the email sent by the first account was actually received by the receiver 
            List<IWebElement> emails = driverNew.FindElements(By.XPath("//*[@class='zF']")).ToList();
            int inititalCount = emails.Count;

            // Refresh the browser to get the newly arrived email
            Thread.Sleep(5000);
            driverNew.Navigate().Refresh();

            Thread.Sleep(5000);

            List<IWebElement> emailsAfterRefresh = driverNew.FindElements(By.XPath("//*[@class='zF']")).ToList();

            Assert.Greater(emailsAfterRefresh.Count, inititalCount);

            IWebElement element = emailsAfterRefresh.LastOrDefault(x => x.Text.StartsWith("qaengineer testing"));
            Assert.AreEqual("qaengineer testing", element.Text);
            element.Click();

            //sign out
            driverNew.FindElement(By.XPath("//span[@class='gb_V gbii']")).Click();
            driverNew.FindElement(By.LinkText("Sign out")).Click();


            Thread.Sleep(2000);
            driverNew.Quit();
        }

        private static void ComposeEmail(string emailReceiver, string emailSubject, string emailBody, IWebDriver driver)
        {
            // Compose Email
            driver.FindElement(By.XPath("//div[contains(text(),'Compose')]")).Click();

            driver.FindElement(By.XPath("//td//img[2]")).Click();
            driver.FindElement(By.ClassName("vO")).SendKeys(emailReceiver);
            driver.FindElement(By.ClassName("aoT")).SendKeys(emailSubject);
            driver.FindElement(By.XPath("//td[@class='Ap']/div[2]/div[@class='Am Al editable LW-avf']")).SendKeys(emailBody);
            driver.SwitchTo().DefaultContent();

            driver.FindElement(By.XPath("//div[text()='Send']")).Click();
        }

        private IWebDriver LoginWithSpecifiedUser(string userEmail)
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");

            IWebDriver driver = new ChromeDriver(options);
            string url = "https://mail.google.com/";

            // Login with the receiver
            driver.Manage().Timeouts().ImplicitWait = new TimeSpan(5000);
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();

            driver.FindElement(By.Id("identifierId")).Clear();
            driver.FindElement(By.Id("identifierId")).SendKeys(userEmail);
            driver.FindElement(By.Id("identifierNext")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys("seleniumtesting");
            driver.FindElement(By.Id("passwordNext")).Click();

            return driver;
        }
    }
}

